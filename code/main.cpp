#include "opencv2/highgui/highgui.hpp"

#include <unistd.h>
#include <thread>
#include <chrono>
#include <fstream>
#include <map>
#include <set>

#include <math.h>
#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <iostream>
#include <unordered_set>
#include <unordered_map>
#include <boost/filesystem.hpp>
#include <queue>
#include <chrono>


#include "opencv2/imgproc/imgproc.hpp"
#include <opencv2/calib3d/calib3d.hpp>

namespace fs = boost::filesystem;

struct CallBackStruct {
    std::vector<cv::Point> movedPoints;
    std::vector<cv::Point> clickedPoints;
};

enum class ButtonType { Clear,
                        Impasse,
                        Next,
                        Text,
                        None};

bool checkThatPointIsInsideRect(const cv::Point& point, const cv::Rect& rect) {
    if (point.x < rect.tl().x) {
        return false;
    }
    if (point.x > rect.br().x) {
        return false;
    }
    if (point.y < rect.tl().y) {
        return false;
    }
    if (point.y > rect.br().y) {
        return false;
    }
    return true;
}

struct EnumClassHash
{
    template <typename T>
    std::size_t operator()(T t) const
    {
        return static_cast<std::size_t>(t);
    }
};

const std::string currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}




struct Config {
    bool randomProblems;
    std::string problemsPath = "./../equations";
};

class ConfigMaker {
public:
    ConfigMaker(const std::string& pathToConfig) {
        try {
            auto lines = getVectorOfLinesFromFile(pathToConfig);
            for (auto& line : lines) {
                line = findWordBetweenQuotes(line);
            }

            configPtr_ = std::make_shared<Config>();
            if (lines[0] == "true") {
                configPtr_->randomProblems = true;
            } else if (lines[0] == "false") {
                configPtr_->randomProblems = false;
            } else {
                throw;
            }

            configPtr_->problemsPath = lines[1];
        } catch (...) {
            throw std::runtime_error("Reading config failed");
        }

    }

    static const Config* getConfig() {
        return configPtr_.get();
    }
private:
    static std::shared_ptr<Config> configPtr_;

    static std::string findWordBetweenQuotes(const std::string& word) {
        auto subWord = word.substr(word.find("\"") + 1);
        subWord = subWord.substr(0, subWord.find("\""));
        return subWord;
    }

    static std::vector<std::string> getVectorOfLinesFromFile(const std::string& pathToFile) {
        std::ifstream infile(pathToFile);
        std::vector<std::string> result;
        std::string line;
        while (std::getline(infile, line))
        {
            result.push_back(line);
        }
        return result;
    }

};
std::shared_ptr<Config> ConfigMaker::configPtr_ = nullptr;


class MainWindow {
public:
    const static cv::Size windowSize_;

    MainWindow() {
        originalWindow_ = cv::imread("../code/background.jpg");
        cv::resize(originalWindow_, originalWindow_, windowSize_, 0, 0, 3);
    }

    void setButton(const cv::Mat& button, const cv::Rect& position, const ButtonType& type) {
        if (buttonPosition_.count(type) != 0) {
            throw std::runtime_error("Already have such button type");
        }
        cv::Mat newButton;
        cv::Rect resultPosition = position & cv::Rect(0, 0, windowSize_.width, windowSize_.height);
        cv::resize(button, newButton, resultPosition.size(), 0, 0, 3);
        buttonPosition_[type] = resultPosition;
        newButton.copyTo(originalWindow_(resultPosition));
    }

    const cv::Mat& getOriginalWindow() const {
        return originalWindow_;
    }

    ButtonType getType(const cv::Point& point) const {
        for (const auto& item : buttonPosition_) {
            if (checkThatPointIsInsideRect(point, item.second)) {
                return item.first;
            }
        }
        return ButtonType::None;
    }

private:
    std::unordered_map<ButtonType, cv::Rect, EnumClassHash> buttonPosition_;
    cv::Mat originalWindow_;

    //    void addText() {
    //        auto textFrame = cv::imread("../code/text.png");
    //        cv::Rect position(0, 0, windowSize_.width, windowSize_.height*0.5);
    //        cv::Mat newTextFrame;
    //        cv::resize(textFrame, newTextFrame, position.size(), 0, 0, 3);
    //        newTextFrame.copyTo(originalWindow_(position));


    //    }
};

const cv::Size MainWindow::windowSize_ = cv::Size{1400, 700};


#define LOG_TEXT Logger::GetInstance().GetStream()
class Logger {
public:
    static void Init(const std::string& logFolder) {
        if (!textFile_.is_open()) {
            videoWriterPtr_ = new cv::VideoWriter(logFolder + "/video.avi", cv::VideoWriter::fourcc('M','J','P','G'), 30, writeVideoSize_);
            textFile_.open(logFolder + "/log.txt");
            output_stream_ = &textFile_;
            LOG_TEXT << "New session in " << logFolder << std::endl;
        }
    }

    static std::ostream& GetStream() {
        *output_stream_ << "[" << currentDateTime() << "] ";
        return *output_stream_;
    }

    static Logger& GetInstance() {
        if (!loggerPtr_) {
            loggerPtr_.reset(new Logger);
        }
        return *loggerPtr_;
    }

    static void writeFrame(cv::Mat frame) {
        cv::resize(frame, frame, writeVideoSize_);
        videoWriterPtr_->write(frame);
    }



private:
    Logger() {

    }

    static const cv::Size writeVideoSize_;
    static std::unique_ptr<Logger> loggerPtr_;
    static std::ofstream textFile_;
    static std::ostream* output_stream_;
    static cv::VideoWriter* videoWriterPtr_;

};
std::unique_ptr<Logger> Logger::loggerPtr_ = nullptr;
std::ostream* Logger::output_stream_ = &std::cout;
std::ofstream Logger::textFile_;
cv::VideoWriter* Logger::videoWriterPtr_ = nullptr;
const cv::Size Logger::writeVideoSize_ = {700, 350};

void onMouse(int evt, int x, int y, int flags, void* param) {
    if(evt == cv::EVENT_MOUSEMOVE) {
        CallBackStruct* ptr = (CallBackStruct*)param;
        ptr->movedPoints.push_back(cv::Point(x,y));
    }
    if(evt == cv::EVENT_LBUTTONDOWN) {
        CallBackStruct* ptr = (CallBackStruct*)param;
        ptr->clickedPoints.push_back(cv::Point(x,y));
    }
}

cv::Mat drawGrid(const MainWindow& window, const cv::Point& startingPoint, int step, int number) {
    auto result = window.getOriginalWindow();
    auto color = cv::Scalar(255, 255, 0);
    int radius = 5;
    for (int ind = 0; ind < number; ++ind) {
        for (int ind_second = 0; ind_second < number; ++ind_second) {
            auto point = startingPoint + step * cv::Point(ind, ind_second);
            if (window.getType(point) == ButtonType::None) {
                cv::circle(result, point, radius, color, -1);
            }

            //            cv::circle(result, startingPoint + step * cv::Point(-ind, ind_second), radius, color, -1);
            //            cv::circle(result, startingPoint + step * cv::Point(ind, -ind_second), radius, color, -1);
            //            cv::circle(result, startingPoint + step * cv::Point(-ind, -ind_second), radius, color, -1);
        }
    }
    return result;
}

cv::Mat drawGrid(const cv::Mat& frame, cv::Point startingPoint, int step, int number) {
    auto result = frame.clone();
    auto color = cv::Scalar(255, 255, 0);
    int radius = 5;
    for (int ind = 0; ind < number; ++ind) {
        for (int ind_second = 0; ind_second < number; ++ind_second) {

            cv::circle(result, startingPoint + step * cv::Point(ind, ind_second), radius, color, -1);
            cv::circle(result, startingPoint + step * cv::Point(-ind, ind_second), radius, color, -1);
            cv::circle(result, startingPoint + step * cv::Point(ind, -ind_second), radius, color, -1);
            cv::circle(result, startingPoint + step * cv::Point(-ind, -ind_second), radius, color, -1);
        }
    }
    return result;
}
int findClosestPoint(int point, int startingPoint, int step) {
    int number = (point - startingPoint) / step;
    std::vector<int> candidates;
    candidates.push_back(startingPoint + (number + 1) * step);
    candidates.push_back(startingPoint + (number) * step);
    candidates.push_back(startingPoint + (number - 1) * step);
    int minInd = 0;
    int minDist = std::numeric_limits<int>::max();
    for (size_t ind = 0; ind < candidates.size(); ++ind) {
        if (abs(candidates[ind] - point) < minDist) {
            minDist = abs(candidates[ind] - point);
            minInd = ind;
        }
    }
    return candidates[minInd];

    //    return  startingPoint + number * step;

    //    if (abs(point - number * step) < abs(point - (number + 1) * step)) {
    //        return  startingPoint + number * step;
    //    } else {
    //        return  startingPoint + (number + 1) * step;
    //    }

}
cv::Point findClosestPoint(cv::Point point, cv::Point startingPoint, int step) {
    return cv::Point(findClosestPoint(point.x, startingPoint.x, step),
                     findClosestPoint(point.y, startingPoint.y, step));
}

std::vector<cv::Point> findClosestSquare(cv::Point point, cv::Point startingPoint, int step) {
    auto closestPoint = findClosestPoint(point, startingPoint, step);
    if (point.x <= closestPoint.x && point.y <= closestPoint.y) {
        std::vector<cv::Point> result;
        result.push_back(closestPoint - cv::Point(step, step));
        result.push_back(closestPoint - cv::Point(0, step));
        result.push_back(closestPoint - cv::Point(0, 0));
        result.push_back(closestPoint - cv::Point(step, 0));
        return result;
    } else if (point.x <= closestPoint.x && point.y > closestPoint.y) {
        std::vector<cv::Point> result;
        result.push_back(closestPoint - cv::Point(step, 0));
        result.push_back(closestPoint - cv::Point(0, 0));
        result.push_back(closestPoint + cv::Point(0, step));
        result.push_back(closestPoint + cv::Point(-step, step));
        return result;
    } else if (point.x > closestPoint.x && point.y > closestPoint.y) {
        std::vector<cv::Point> result;
        result.push_back(closestPoint - cv::Point(0, 0));
        result.push_back(closestPoint + cv::Point(step, 0));
        result.push_back(closestPoint + cv::Point(step, step));
        result.push_back(closestPoint + cv::Point(0, step));
        return result;
    } else {
        std::vector<cv::Point> result;
        result.push_back(closestPoint - cv::Point(0, step));
        result.push_back(closestPoint + cv::Point(step, -step));
        result.push_back(closestPoint + cv::Point(step, 0));
        result.push_back(closestPoint - cv::Point(0, 0));
        return result;
    }

    return std::vector<cv::Point>();
}
struct Line {
    double a;
    double b;
    double c;
};

Line lineFromPoints(cv::Point p, cv::Point q) {
    double a = q.y - p.y;
    double b = p.x - q.x;
    double c = q.x * p.y - p.x * q.y;
    return {a, b, c};
}

double distToLineFromPoint(cv::Point p, Line line) {
    return fabs(line.a * p.x + line.b * p.y + line.c)/(sqrt(line.a * line.a + line.b * line.b));
}

struct pairHash
{
    template <class T1, class T2>
    std::size_t operator() (const std::pair<T1, T2> &pair) const
    {
        return std::hash<T1>()(pair.first) ^ std::hash<T2>()(pair.second);
    }
};

typedef std::pair<cv::Point, cv::Point> pointPair;

struct pointPairHash {
    std::size_t operator() (const pointPair &pair) const
    {
        return std::hash<int>()(pair.first.x) ^
                std::hash<int>()(pair.first.y) ^
                std::hash<int>()(pair.second.x) ^
                std::hash<int>()(pair.second.y);
    }
};

//pointPair getClosestPointPair(const cv::Point& point, const cv::Point& startingPoint, int step) {
//    auto square = findClosestSquare(point, startingPoint, step);

//    std::vector<Line> lines;
//    std::vector<cv::Point> leftPoints;
//    std::vector<cv::Point> rightPoints;

//    for (size_t ind_first = 0; ind_first < square.size(); ++ind_first) {
//        for (size_t ind_second = 0; ind_second < square.size(); ++ind_second) {
//            if (ind_first > ind_second) {
//                leftPoints.push_back(square[ind_first]);
//                rightPoints.push_back(square[ind_second]);
//                lines.push_back(lineFromPoints(square[ind_first], square[ind_second]));
//            }
//        }
//    }

//    std::vector<double> dists;
//    for (const auto& line : lines) {
//        dists.push_back(distToLineFromPoint(point, line));
//    }
//    auto min_it = std::min_element(dists.begin(), dists.end());
//    return std::make_pair(leftPoints[min_it - dists.begin()], rightPoints[min_it - dists.begin()]);
//}

typedef std::unordered_set<pointPair, pointPairHash> Segments;
enum class SegmentType { Number,
                         Operator,
                       };

bool checkOrthogonality(const pointPair& lhs, const pointPair& rhs) {
    auto left = lhs.first - lhs.second;
    auto right = rhs.first - rhs.second;
    return left.x * right.x + left.y * right.y == 0;
}

SegmentType getType(const pointPair& segment, const Segments& currentSegments) {
    if (segment.first.y == segment.second.y) {
        return SegmentType::Operator;
    }

    if (segment.first.x == segment.second.x) {
        for (const auto& item : currentSegments) {
            if (item.first.y == item.second.y &&
                    item.first.x < segment.first.x &&
                    item.second.x > segment.first.x) {
                return SegmentType::Operator;
            }
        }
    }
    return SegmentType::Number;
}

cv::Mat drawSegments(const Segments& segments) {
    auto result = cv::Mat(MainWindow::windowSize_, CV_8UC3, cv::Scalar(255, 255, 255));
    for (const auto& segment : segments) {
        cv::line(result, segment.first, segment.second, cv::Scalar(0, 0, 0), 3);
    }
    return result;
}

class Equation {
public:


    static std::queue<Equation> makeQueue(const cv::Point& startingPoint, int gridStep) {
        equationsPath_ = ConfigMaker::getConfig()->problemsPath;
        std::set<int> allNumbers;

        for (const auto & entry : fs::directory_iterator(equationsPath_)) {
            if (!fs::is_directory(entry)) {
                continue;
            }
            int number = -1;
            try {
                number = std::stoi(entry.path().string().substr(equationsPath_.size() + 1));
            } catch (...) {
                std::cout << "Bad file number" << std::endl;
            }
            allNumbers.insert(number);
        }

        std::vector<int> numberVec;
        for (const auto& id : allNumbers) {
            numberVec.push_back(id);
        }

        if (ConfigMaker::getConfig()->randomProblems) {
            std::srand (unsigned ( std::time(0)));
            auto biggest = numberVec.back();
            numberVec.pop_back();
            std::random_shuffle (numberVec.begin(), numberVec.end());
            numberVec.push_back(biggest);
        }

        std::queue<Equation> result;
        for (const auto& number : numberVec) {
            auto buf = Equation(equationsPath_ + "/" + std::to_string(number));

            buf.centerSegments(startingPoint, gridStep);
            buf.name_ = std::to_string(number);
            result.push(buf);
        }

        return result;
    }

    static void saveToFile(const Segments& segments) {
        if (!fs::exists(equationsPath_)) {
            fs::create_directory(equationsPath_);
        }

        int maxUsedNumber = 0;
        for (const auto & entry : fs::directory_iterator(equationsPath_)) {
            int number = -1;
            try {
                number = std::stoi(entry.path().string().substr(equationsPath_.size() + 1));
            } catch (...) {
                std::cout << "Bad file number" << std::endl;
            }
            maxUsedNumber = std::max(maxUsedNumber, number);
        }

        fs::create_directory(equationsPath_ + "/" + std::to_string(maxUsedNumber + 1));

        std::ofstream file(equationsPath_ + "/" + std::to_string(maxUsedNumber + 1) + "/segments.txt");
        for (const auto& segment : segments) {
            file << segment.first.x << " " << segment.first.y << " " << segment.second.x << " " << segment.second.y << std::endl;
        }

        cv::imwrite(equationsPath_ + "/" + std::to_string(maxUsedNumber + 1) + "/picture.png",
                    drawSegments(segments));
    }

    const Segments& getSegments() const {
        return segments_;
    }

    const std::string& getName() const {
        return name_;
    }

private:
    static std::string equationsPath_;

    explicit Equation(const std::string& folderPath) {
        if (fs::exists(folderPath)) {
            std::ifstream infile(folderPath + "/segments.txt");
            std::string line;
            while (std::getline(infile, line))
            {
                std::istringstream iss(line);
                cv::Point leftPoint, rightPoint;
                if (!(iss >> leftPoint.x >> leftPoint.y >> rightPoint.x >> rightPoint.y)) { break;}
                segments_.insert(std::make_pair(leftPoint, rightPoint));
                segments_.insert(std::make_pair(rightPoint, leftPoint));
            }
        }
    }


    cv::Rect getBoundingRect() const {
        std::vector<cv::Point> points;
        for (const auto& item : segments_) {
            points.push_back(item.first);
            points.push_back(item.second);

        }
        return cv::boundingRect(points);
    }

    void shiftSegments(const cv::Point& shift) {
        auto topLeft = getBoundingRect().tl();
        Segments segments;
        for(auto item : segments_) {
            item.first = item.first - topLeft + shift;
            item.second = item.second - topLeft + shift;
            segments.insert(item);
            segments.insert(std::make_pair(item.second, item.first));
        }
        std::swap(segments, segments_);
    }

    void centerSegments(const cv::Point& startingPoint, int gridStep) {
        auto rect = getBoundingRect();
        if (rect.size().width > MainWindow::windowSize_.width ||
                rect.size().height > MainWindow::windowSize_.height) {
            throw std::runtime_error("Bad equation size");
        }

        auto xShift = (MainWindow::windowSize_.width - rect.size().width) / 2;
        auto shiftPoint = cv::Point{xShift, static_cast<int>(MainWindow::windowSize_.height*0.4)};
        shiftPoint = findClosestPoint(shiftPoint, startingPoint, gridStep);
        shiftSegments(shiftPoint);
    }



    Segments segments_;
    std::string name_;
};

std::string Equation::equationsPath_ = "./../equations";

//class

void AddPoints(const cv::Point& leftPoint, const cv::Point& rightPoint,
               std::vector<cv::Point>& leftPoints,
               std::vector<cv::Point>& rightPoints,
               std::vector<Line>& lines) {
    leftPoints.push_back(leftPoint);
    rightPoints.push_back(rightPoint);
    lines.push_back(lineFromPoints(leftPoint, rightPoint));
}
pointPair getClosestPointPair(const cv::Point& point, const cv::Point& startingPoint, int step) {
    auto square = findClosestSquare(point, startingPoint, step);
    assert(square[0].x < square[1].x);
    assert(square[0].y == square[1].y);
    assert(square[1].y < square[2].y);
    assert(square[1].x == square[2].x);
    assert(square[3].x < square[2].x);
    assert(square[3].y == square[2].y);
    assert(square[0].y < square[3].y);
    assert(square[0].x == square[3].x);
    std::vector<Line> lines;
    std::vector<cv::Point> leftPoints;
    std::vector<cv::Point> rightPoints;

    AddPoints(square[0], square[0] + cv::Point(0, 2 * step), leftPoints, rightPoints, lines);
    AddPoints(square[1], square[1] + cv::Point(0, 2 * step), leftPoints, rightPoints, lines);

    AddPoints(square[0], square[0] + cv::Point(2 * step, 0), leftPoints, rightPoints, lines);
    AddPoints(square[3], square[3] + cv::Point(2 * step, 0), leftPoints, rightPoints, lines);

    AddPoints(square[0], square[0] + cv::Point(step, 2 * step), leftPoints, rightPoints, lines);
    AddPoints(square[1], square[1] + cv::Point(-step, 2 * step), leftPoints, rightPoints, lines);



    //    AddPoints(square[0], square[0] + cv::Point(0, 4 * step), leftPoints, rightPoints, lines);
    //    AddPoints(square[1], square[1] + cv::Point(0, 4 * step), leftPoints, rightPoints, lines);
    //    AddPoints(square[0], square[0] + cv::Point(4 * step, 0), leftPoints, rightPoints, lines);
    //    AddPoints(square[3], square[3] + cv::Point(4 * step, 0), leftPoints, rightPoints, lines);
    //    AddPoints(square[0], square[0] + cv::Point(step, 4 * step), leftPoints, rightPoints, lines);
    //    AddPoints(square[1], square[1] + cv::Point(-step, 4 * step), leftPoints, rightPoints, lines);


    std::vector<double> dists;
    for (const auto& line : lines) {
        dists.push_back(distToLineFromPoint(point, line));
    }
    auto min_it = std::min_element(dists.begin(), dists.end());
    return std::make_pair(leftPoints[min_it - dists.begin()], rightPoints[min_it - dists.begin()]);
}

class Statistics {
public:
    Statistics() {
        problemStartTime_ = std::chrono::steady_clock::now();
    }

    uint64_t getTimeDifference () const {
        return std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()
                                                                     - problemStartTime_).count();
    }

    size_t operatorClicks = 0;
    size_t numberClicks = 0;
    std::chrono::steady_clock::time_point problemStartTime_;

};

std::string PrepareResultsFolder() {
    const std::string resultsFolder = "./../../insight_results";
    if (!fs::exists(resultsFolder)) {
        fs::create_directory(resultsFolder);
    }
    fs::create_directory(resultsFolder + "/" + currentDateTime());

    return resultsFolder + "/" + currentDateTime();
}

int main() {
    ConfigMaker("../config.txt");

    Logger::Init(PrepareResultsFolder());

    MainWindow window;
    //    window.setButton(cv::imread("../autonomusDrone/clear.png"), cv::Rect(0, 0, 300, 150), ButtonType::Clear);
    window.setButton(cv::imread("../code/clear.png"), cv::Rect(800, 600, 150, 75), ButtonType::Clear);

    //    window.setButton(cv::imread("../autonomusDrone/impasse.png"), cv::Rect(300, 0, 300, 150), ButtonType::Impasse);
    window.setButton(cv::imread("../code/impasse.png"), cv::Rect(1000, 600, 150, 75), ButtonType::Impasse);

    //    window.setButton(cv::imread("../autonomusDrone/next.png"), cv::Rect(600, 0, 300, 150), ButtonType::Next);
    window.setButton(cv::imread("../code/next.png"), cv::Rect(1200, 600, 150, 75), ButtonType::Next);


    window.setButton(cv::imread(ConfigMaker::getConfig()->problemsPath + "/text.png"), cv::Rect(0, 0, MainWindow::windowSize_.width, MainWindow::windowSize_.height*0.3), ButtonType::Text);

    //    cv::imshow("Main window", drawGrid(window, cv::Point(0, 0), 50, 40));
    //    cv::waitKey(0);
    //    return 0;
    CallBackStruct item;


    cv::namedWindow("Output Window");

    auto frame = window.getOriginalWindow();
    //    cv::Mat frame = cv::imread("../autonomusDrone/sheet.png");
    auto startingPoint = cv::Point(0, 0);
    int gridStep = 50;
    auto equationQueue = Equation::makeQueue(startingPoint, gridStep);

    const auto movingColour = cv::Scalar(11, 39,69);
    const auto staticColour = cv::Scalar(190, 190, 190);

    //    cv::imshow("GRID", drawGrid(frame, startingPoint, gridStep, 10));
    //    cv::waitKey(0);
    cv::setMouseCallback("Output Window", onMouse, &item);

    auto cleanFrame = frame.clone();
    //    cv::imshow("Output Window", frame);

    //    cv::waitKey(1);

    if (equationQueue.empty()) {
        std::cout << "No equations to solve" << std::endl;
        return 0;
    }
    std::unordered_set<pointPair, pointPairHash> presentSegments;
    Statistics current_stats;
    auto current_problem = equationQueue.front();
    LOG_TEXT << "New problem with number " << current_problem.getName() << std::endl;
    presentSegments = current_problem.getSegments();
//    presentSegments.clear();
    auto lastMovedPoint = cv::Point(-1, -1);
    while(1)
    {
        //                frame = cleanFrame.clone();
        cv::imshow("Output Window", frame);

        frame = cleanFrame.clone();
        cv::Mat savedFrame = frame.clone();
        bool needSave = false;
        for (const auto& segment : presentSegments) {
            //            cv::line(frame, segment.first, segment.second, cv::Scalar(230, 216, 173), 10);
            cv::line(frame, segment.first, segment.second, staticColour, 10);
            cv::line(savedFrame, segment.first, segment.second, staticColour, 10);

        }


        if (item.movedPoints.size() > 0) {
            needSave = true;
            cv::circle(savedFrame, item.movedPoints.front(), 10, cv::Scalar(255, 255, 0), -1);

            auto lastPoint = item.movedPoints.back();

            if (window.getType(lastPoint) == ButtonType::None) {
                lastMovedPoint = lastPoint;
                auto pp = getClosestPointPair(lastPoint, startingPoint, gridStep);
                if (window.getType(pp.first) == ButtonType::None &&
                        window.getType(pp.second) == ButtonType::None) {
                    cv::line(frame, pp.first, pp.second, movingColour, 3);
                    cv::line(savedFrame, pp.first, pp.second, movingColour, 3);

                }
                item.movedPoints.clear();
            }


        } else {
            if (lastMovedPoint.x != -1 && lastMovedPoint.y != -1) {
                auto lastPoint = lastMovedPoint;
                auto pp = getClosestPointPair(lastPoint, startingPoint, gridStep);
                if (window.getType(pp.first) == ButtonType::None &&
                        window.getType(pp.second) == ButtonType::None) {
                    cv::line(frame, pp.first, pp.second, movingColour, 3);
                    cv::line(savedFrame, pp.first, pp.second, movingColour, 3);

                }
                item.movedPoints.clear();
            }
        }
        if (item.clickedPoints.size() > 0) {
            needSave = true;
            cv::circle(savedFrame, item.clickedPoints.front(), 20, cv::Scalar(0, 255, 0), -1);
            auto lastPoint = item.clickedPoints.back();
            if (window.getType(lastPoint) == ButtonType::Clear) {
                LOG_TEXT << "CLEAR clicked. Total time from problem beginning " << current_stats.getTimeDifference() << std::endl;

                //                std::cout << "CLEARED SEGMENTS" << std::endl;
//                                Equation::saveToFile(presentSegments);
//                                presentSegments.clear();

                presentSegments = equationQueue.front().getSegments();


            }
            if (window.getType(lastPoint) == ButtonType::Impasse) {


                LOG_TEXT << "IMPASSE clicked. Total time from problem beginning " << current_stats.getTimeDifference() << std::endl;
            }

            if (window.getType(lastPoint) == ButtonType::Next) {
                equationQueue.pop();
                if (equationQueue.empty()) {
                    return 0;
                } else {
                    LOG_TEXT << "NEXT clicked. Total time from problem beginning " << current_stats.getTimeDifference() << std::endl;
                    LOG_TEXT << "Finished problem number: " << current_problem.getName() << ". Stats : [NUMBER, OPERATOR]" << " [" << current_stats.numberClicks << ", " << current_stats.operatorClicks << "]" << std::endl;

                    current_stats = Statistics();
                    current_problem = equationQueue.front();
                    presentSegments = current_problem.getSegments();
                    LOG_TEXT << "New problem with number " << current_problem.getName() << std::endl;

                }
            }

            if (window.getType(lastPoint) == ButtonType::None) {
                auto pairFirst = getClosestPointPair(lastPoint, startingPoint, gridStep);
                                if (getType(pairFirst, presentSegments) == SegmentType::Number) {
                                    current_stats.numberClicks++;
                                } else {
                                    current_stats.operatorClicks++;
                                }

                auto pairSecond = std::make_pair(pairFirst.second, pairFirst.first);
                if (presentSegments.count(pairFirst) == 0) {
                    presentSegments.insert(pairFirst);
                } else {
                    presentSegments.erase(pairFirst);
                }
                if (presentSegments.count(pairSecond) == 0) {
                    presentSegments.insert(pairSecond);
                } else {
                    presentSegments.erase(pairSecond);
                }
            }
            item.clickedPoints.clear();

        }
        if (needSave) {
            Logger::writeFrame(savedFrame);
        }

        cv::waitKey(1);
    }
    return 0;
}
